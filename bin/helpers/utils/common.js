'use strict';

const getLastFromURL = async (url) => {
    let name = decodeURI(url).split('/').pop();
    name = name.replace(/(\r\n|\n|\r)/gm,"");
    return String(name);
}

const isPostiveNumber = (parameter) => {
    let res = false;
    if (parameter != null && !isNaN(parameter) && parameter > 0) {
      res = true;
    }
    return res;
  }

module.exports = {
    getLastFromURL: getLastFromURL,
    isPostiveNumber: isPostiveNumber
}