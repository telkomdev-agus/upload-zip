'use strict'

const rp = require('request-promise');
const request = require('request');
const nconf = require('nconf');
const wrapper = require('../../../../helpers/utils/wrapper');
const config = require('../../../../infra/configs/global_config');
const validate = require('validate.js');
const aws = require('aws-sdk');
const ba64 = require("ba64");
const uuidv4 = require('uuid/v4');
const fs = require('fs');
const path = require('path');
const randomstring = require('randomstring');
const util = require('../../../../helpers/utils/common');
const ObjectId = require('mongodb').ObjectId;
const date = require('date-and-time');
const unzip = require('unzip');


class UploadZip {

    async upload(data){

        fs.createReadStream(data.zip)
        .pipe(unzip.Parse())
        .on('entry', function (entry) {
            var fileName = entry.path;
            var type = entry.type; // 'Directory' or 'File'
            var size = entry.size;
            if (fileName === "this IS the file I'm looking for") {
            entry.pipe(fs.createWriteStream('output/path'));
            } else {
            entry.autodrain();
            }
        });

    }

}

module.exports = UploadZip;


