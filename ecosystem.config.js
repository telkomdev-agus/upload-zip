'use strict';

module.exports = {
    apps: [
        {
            name: 'bk-api',
            script: 'index.js',
            instances: '3',
            exec_mode: 'cluster',
            env: {
                COMMON_VARIABLE: 'true'
            }
        }
    ],
    deploy: {
        production: {
            user: 'ubuntu',
            key: '~/.ssh/node_ec2.pem',
            host: 'ec2-13-229-30-110.ap-southeast-1.compute.amazonaws.com',
            ref: 'origin/master',
            repo: 'git@gitlab.com:telkomdev/belajarcoding-apis.git',
            path: '/home/ubuntu',
            'post-deploy': 'npm install && pm2 startOrRestart ecosystem.config.js --force',
            env: {
                NODE_ENV: 'production',
                SERVICE_HOST: '0.0.0.0',
                SERVICE_PORT: 3000,
                ADMIN_SERVICE_PORT: 3001,
                AUTH0_API_AUDIENCE: 'https://telkomdev.au.auth0.com/api/v2/',
                AUTH0_CLIENT_ID: 'GGZwvi9yPq4IkgGj1E5ztVF5niTYMSR5',
                AUTH0_CLIENT_SECRET: 'kEP5_FH-0wnTDtKYAhyT_IldvP77PbvylgAfrTrCODjgXItuwgAT5nMWEQBQA7-c',
                AUTH0_DOMAIN: 'https://telkomdev.au.auth0.com',
                AUTH0_PUBLICKEY_PATH: 'public.pem',
                AUTH0_REQUEST_TIMEOUT: 15000,
                LOG_LEVEL: 'debug',
                DB_INSTANCES: 'ec2-54-254-234-183.ap-southeast-1.compute.amazonaws.com:27017',
                DB_NAME: 'admin',
                DB_USER: 'admin',
                DB_PASSWORD: 'passwordAdmin',
                SEND_GRID_API_KEY: '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',
                SEND_GRID_FROM_ADDRESS: 'no-reply@logtan.com',
                SEND_GRID_ADMIN_ADDRESS: 'telkom_dev@helio.id',
                LOGGLY_TOKEN: 'ed088545-2e19-48df-b194-0b819dc1b565',
                LOGGLY_DOMAIN: 'playcourt',
                LOGGLY_BUFFER_LENGTH: 0,
                LOGGLY_BUFFER_TIMEOUT: 0,
                LOGGLY_GROUP_TAG: 'production',
                BUILD_ENV: 'production'
            }
        }
    }
};